// Setting up the dependencies

const express = require("express"); //we are getting all the dependencies // accessing express
const mongoose = require('mongoose');

// allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute"); // way of importing

// Server set up 

const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database connection 
// Connecting to MongoDB Atlas

mongoose.connect("mongodb+srv://mikaellajdeleon:zNOIjPKcWp72KCSD@wdc028-course-booking.vww2uoq.mongodb.net/?retryWrites=true&w=majority", {
	useNewUrlParser: true, 
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', console.log.bind(console, 'MongoDB connection error.'))
db.once('open', ()=> console.log('Now connected to MongoDB Atlas!'))


// Add the task route
// Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route

app.use("/tasks", taskRoute)
// Server listening

if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app; // way of exporting

// app.js is connected to taskRoute 
// taskRoute is connected to taskController
// taskController is connected to task.js

// server <= route <= controller <= model