// Contains all the endpoints for our application
// We seperate the routes such that "app.js" only contains information on the server. 

// We need to use express' Router() function to achieve this

const express = require("express");

// creates a Router instance that functions as a middleware and routing system 
// Allows access to HTTP method middlewares that makes it easier to create routes for our application. 

const router = express.Router();

const taskController = require("../controllers/taskController");
// [Section] Routes
// Routes are responsible for defining the URI's that our client accesses and the corresponding controller functions that will be used when a route is accessed
// They invoke the controller function from the controller files
// All business logic is done in the controller 

// Route to get all the tasks 
// Expects to receive GET request at the URL '/tasks'
// Whole URL is at "http;//localhost:4000/tasks"

router.get("/", (req, res) => {
	// Invokes the "getAllTask" function from the "taskController.js" file and sends the results back to the client/postman

	// resultFromController is only used here to make the code easier to understand but it is common practice to use the shorthand parameter name for a result using "result/res"
	taskController.getAllTask().then(resultFromController => res.send(resultFromController));
})

// ACTIVITY "GETTING A SPECIFIC TASK"

router.get("/:id", (req,res) =>{

	taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route to create a new task 
// This route expects to receive a POST request at the URL "/tasks"
// The whole URL is "http://localhost4000/tasks"

router.post("/", (req, res) =>{
	// "createTask" function needs the data from the request body, so we need to supply it to the function

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})
// Route to delete a task
// Route expects to receive a DELETE request at the URL "tasks/:id"
// Whole URL is at "http://localhost:4000/tasks/:id"
// Task ID is obtained from the URL is denoted by the ":id" identifier in the route
// the colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the URL 
// The word that comes after the colon(:) symbol will be the name of the URL parameter
// The :id is a wild card where you can put any value, creates link between "id" parameter in the URL and the value provided in the URL
router.delete("/:id", (req,res) =>{

	// URL parameter values are accessed via the request object "params" property
	// Property name of this object will match the given URL parameter name
	// id is the name of the parameter
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route to Update Task
/*
Expects to receive a PUT request at the URL "/tasks/:id"
Whole URL is "http://localhost:4000/tasks/id"
*/
router.put("/:id", (req,res) =>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// [ACTIVITY] UPDATE STATUS
router.put("/:id/complete", (req,res) =>{
	taskController.updateStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});


// Use module.export to export the router object to use in the app.js
module.exports = router;