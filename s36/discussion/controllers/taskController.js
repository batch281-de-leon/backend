// Controllers contain the functions and business logic of our express js application
// All operations it can do will be placed in this file

// Uses the "require" directive to allow access to the "Task" model which allows us to access the Mongoose methods to perform CRUD functions

// Allows us to use the contents of the "task.js" file in the "models" folder

const Task = require("../models/task");

// Controller function for getting all the tasks
// Defines the functions to be used in the "taskRoute.js" file and exports these functions

module.exports.getAllTask = () =>{

	// return statement returns the result of mongoose method "find" back to the "taskRoute.js" file which invokes this function when the "/tasks" routes is accessed. 
	// then method is used to wait for the Mongoose "find" method to finish before sending the result back to the route.
	return Task.find({}).then(result =>{
		return result;
	})
}

// [ACTIVITY] GETTING SPECIFIC TASK
module.exports.getSpecificTask = (taskId) =>{
	return Task.findById(taskId).then((specificTask,err)=>{
		if(err){
			console.log(err);
			return false;
		} else{
			return specificTask
		}
	})
}

// Controller function for creating a task
// Req.body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed "requestBody" parameter in the controller file. 

module.exports.createTask = (requestBody) => {

	// creates task object based on the mongoose model "Task"

	let newTask = new Task({

		// Sets the "name" property with the value received from postman
		name : requestBody.name
	})

	// saves the newly created "newTask" object in the MongoDB database
	// the "then" method waits until task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client/postman

	// then method will accept the following 2 arguments
		// First parameter will store the result returned by the Mongoose "save" method
		// 2nd parameter will store the "error" object

	return newTask.save().then((task, error) =>{
		if(error) {
			console.log(error)
			return false

			// since the ff return statement is nested within the "then" method chained to the "save" method, they do not prevent eachother from executing code
			// else statement will no longer be evaluated
		} else {
			return task;
		}
	})
}

// Controller function for deleting a task 

// Task ID retrieved from the "req.params.id" property coming from the client is named as a "taskId" parameter in the controller file
module.exports.deleteTask = (taskId) =>{
	// the "findByIdAndRemove' mongoose method that will look for a task with the same id provided from the URL and remove/delete the document 
	return Task.findByIdAndRemove(taskId).then((removedTask,err)=>{
		if(err){
			console.log(err);
			return false;
		} else{
			return removedTask
		}
	})
}

// Controller function for updating a task 

// taskId retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file. 

// Updates to be applied to the documents retrieved from the "req.body" property coming from the client is renamed as "newContent" 
module.exports.updateTask = (taskId, newContent) => {

	// findById will search for the specific ID
	// the return statement returns the result of the Mongoose method "findById" back to the 'taskRoute.js' file which invokes this function
	return Task.findById(taskId).then((result,error) =>{
		if(error){
			console.log(error);
			return false;
		} 
		// Results by the "findById" method will be stored in the "result" parameter. 
		// It's "name" property will be reassigned the value of the "name" received from the request
			result.name = newContent.name;

			// saves the updated object in the MongoDB database
			// Document already exists in the database and was stored in "result" parameter
			// Save method is from mongoose which is used to update existing document with the changes we applied. 
			return result.save().then((updateTask, saveErr) =>{
				if(saveErr){
					console.log(saveErr);
					return false;
				} else {
					return updateTask
				}
			})
	})
}

// [ACTIVITY] UPDATE STATUS

module.exports.updateStatus = (taskId, newStatus) => {
    return Task.findById(taskId).then((result, error) => {
        if(error){
            console.log(error);
            return false;
        }

        result.status = newStatus.status

        return result.save().then((updateStatus, saveErr) => {
            if(saveErr){
                console.log(saveErr);
                return false
            } else {
                return updateStatus
            }
        })
    })
}
