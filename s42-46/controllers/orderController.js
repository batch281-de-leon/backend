const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");

const auth = require("../auth");

// Create Order (Non-Admin)
module.exports.createOrder = async (req, res) => {
	const { productId, quantity } = req.body;
	const { isAdmin, id: userId } = auth.decode(req.headers.authorization);

	if ( !productId || !quantity ) {
		return res.status(400).send('All fields must be filled');
	}

	try {
		const product = await Product.findById(productId);
		if (!product) {
			return res.status(404).send("Product not found");
		}

		if (!product.isActive) {
			return res.status(403).send("Product is not active");
		}

		if (!isAdmin) {
			const newOrder = new Order({
				userId,
				products: [ { productId, quantity } ],
				totalAmount: quantity * product.price
			});

			await newOrder.save();
			return res.send(newOrder);
		} else {
			return res.status(403).send("You are an admin");
		}
	} catch (error) {
		console.error("Error adding order:", error);
		return res.status(500).send("Error adding order");
	}
};


