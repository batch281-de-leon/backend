const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")
const auth = require("../auth");

// Routes for checking if the user's already exist in the database
router.get("/checkEmail", userController.checkEmailExist);

// Routes for user registration
router.post("/register", userController.registerUser);

// Routes for user authentication
router.post("/login", userController.loginUser)


// Routes for getting a specific user's details

router.get("/details", auth.verify, userController.getProfile)

// Routes enrolling a user to a course
router.post("/enroll", auth.verify, userController.enroll)

module.exports = router;
