//  Using require directive to load http module
let http = require("http"); 

// Creating a server
http.createServer(function (request, response){
	// returning what type of response being thrown to the client
	response.writeHead(200, {'Content-Type': 'text/plain'});
	// response.writeHead(statusCode[, statusMessage][, headers]);

	//  send the response with the text content 'Hello, world!'
	response.end('Hello, world!');
}).listen(4000);

// When server is running, console will print the message:
console.log('Server is running at localhost:4000');

