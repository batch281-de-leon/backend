const http = require("http");

// Create a variable 'port' to store the port number

const port = 3000;

const app = http.createServer((request, response) => {
	// Accessing the 'greeting' route returns a message of "Hello, world!"
	if(request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to the login page.');
	} 
	else if (request.url !== '/login') {
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end("I'm sorry. The page you are looking for cannot be found.");
	}
});

app.listen(port);

console.log(`Server now accesible at localhost:${port}.`);