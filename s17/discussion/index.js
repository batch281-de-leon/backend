function printName (){
	console.log("My name is Ella");
};

printName();

// Function declaration vs expression

// Function Declaration

function declaredFunction(){
	console.log("Hello, world from declaredFunction()");
};

declaredFunction();

// Function expression 

let variableFunction = function(){
	console.log("Hello again!");
};

variableFunction();
// Reassign
declaredFunction = function(){
	console.log ("Updated declaredFunction");
};

declaredFunction();

const constantFunc = function(){
	console.log("Initialized with const!");
};

constantFunc();

/*constantFunc = function(){
	console.log("Cannot be re-assign")
};

constantFunc();*/

// Function Scoping - accessibility of variables

/*Three types of scope
1. Local/Block Scope
2. Global Scope
3. Function Scope 

*/

{
	let localVar = "Alonzo Mattheo";
	console.log(localVar);
}

let globalVar = "Aizaac Ellis";


console.log(globalVar);

function showNames()
{
	// Function scope variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();

// Nested Functions 

function myNewFunction(){
	let name = 'Jane';
		
	function nestedFunction(){
		let nestedName = 'John';
		console.log(nestedName);
	}
	nestedFunction();
};

myNewFunction();

// Function and Global Scope Variables
 
 // Global scoped variable

let globalName = 'Joy';

function myNewFunction2(){
	let nameInside = 'Kenzo';
	console.log(globalName)
	console.log(nameInside)
};

myNewFunction2();

// Using Alert()

alert("Hello, world!")

function showSampleAlert(){
	alert("Hello, user!");
};

showSampleAlert();

// Using Prompt ()

let samplePrompt = prompt("Enter your name: ");

console.log("Hello, " + samplePrompt);

function printWelcomeMessage(){
	let firstName = prompt("Enter your first name: ")
	let lastName = prompt("Enter your last name: ")	

	console.log ("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!")
}

printWelcomeMessage();

// Function naming conventions - should be definitive of the task it will perform
// avoid generic names to avoid confusion within the code 
// avoid pointless and inappopriate function names
// name functions using camel casing
// Do not use JS reserved



