// CRUD Operations

// Create 
// Inserting one collection
db.users.insertOne({
	"firstName": "Jane",
	"lastName" : "Doe",
	"age": 21,
	"contact": {
		"phone": "912830340",
		"email": "janedoe@gmail.com"
	},
	"course": ["CSS", "JavaScript", "Python"],
	"department": "none"
});

// Insert many collections

db.users.insertMany([	
	{
	"firstName": "Stephen",
	"lastName" : "Hawking",
	"age": 76,
	"contact": {
		"phone": "912830340",
		"email": "stephenh@gmail.com"
	},
	"course": ["React", "PHP", "Python"],
	"department": "none"
	},
	{
	"firstName": "Neil",
	"lastName" : "Armstrong",
	"age": 82,
	"contact": {
		"phone": "912830340",
		"email": "neilarms@gmail.com"
	},
	"course": ["React", "Laravel", "Sass"],
	"department": "none"
	}

]);

// Finding Documents (Read)
// Find 

// Retrieving all documents

db.users.find();
// Retrieving single documents
db.users.find({"firstName": "Stephen"});
db.users.find({"_id" : ObjectId("6466170f36501bb401594dc4")});

// Retrieving documents with multiple parameters
db.users.find({"lastName": "Armstrong", "age":82});

// Updating documents (Update)

// Update a single documents to update

db.users.insertOne({
	"firstName": "Test",
	"lastName" : "Test",
	"age": 0,
	"contact": {
		"phone": "000000000",
		"email": "test@gmail.com"
	},
	"course": [],
	"department": "none"
});

db.users.updateOne(
	{"firstName": "Test"},
	{$set: { 
	"firstName": "Bill",
	"lastName" : "Gates",
	"age": 65,
	"contact": {
		"phone": "912830343",
		"email": "bill@gmail.com"
	},
	"course": ["PHP", "Laravel", "HTML"],
	"department": "Operations", 
	"status": "active"
		}

	}
);

// updating multiple documents

db.users.updateMany(
	{"department": "none"},
	{
		$set: {"department": "HR"}
	}
);

// Replace one

db.users.replaceOne(
{ "firstName": "Bill"},
{	"firstName": "Bill",
	"lastName" : "Gates",
	"age": 65,
	"contact": {
		"phone": "912830343",
		"email": "bill@gmail.com"
	},
	"course": ["PHP", "Laravel", "HTML"],
	"department": "Operations"}
);

// Creating a document to delete
db.users.insertOne({
	"firstName": "Test"
});

// Deleting a single document

db.users.deleteOne({
	"firstName": "Test"
});

// Query an embedded document

db.users.find({
	"contact.phone":"912830343"

});