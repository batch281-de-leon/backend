// Basic Express Server

const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

app.use(express.json());
// Allows the app to read data from forms
app.use(express.urlencoded({extended:true}));

// MongoDB connection 
// Connecting to MongoDB Atlas 
mongoose.connect("mongodb+srv://mikaellajdeleon:zNOIjPKcWp72KCSD@wdc028-course-booking.vww2uoq.mongodb.net/b281_to-do?retryWrites=true&w=majority", 
	{
	useNewUrlParser: true,
	useUnifiedTopology: true
	}
);


// Set notification for connection success or failure 

let db = mongoose.connection;

// If a connection error occured, output in the console. 

db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output in the console

db.once("open", () => console.log("We're connected to the cloud database"));

// Mongoose Schemas

const taskSchema = new mongoose.Schema({
	name: String, 
	status: {
		type: String, 
		default : "pending"
	}
});

const userSchema = new mongoose.Schema({
  username: String,
  password: String,
});


// Models [Task Models]
const Task = mongoose.model("Task", taskSchema);
// Allows the app to read json data

const User = mongoose.model("User", userSchema);


// Creation of todo list routes

// Creating new task 

app.post("/tasks", (req, res) => {
	
	Task.findOne({name : req.body.name}).then((result, err) => {
		// If a document was found and document's name matches the information from the client
		if(result !== null && result.name === req.body.name){
		// Return a message to the client/Postman
		return res.send("Duplicate task found");
		}
		// If no document was found
		else{
			// Create a new task and save it to the database
			let newTask=new Task({
				name: req.body.name
			});

			newTask.save().then((savedTask, saveErr) => {
				// If there are error in saving, 
				if(saveErr){
					return console.error(saveErr);
				}
				// no error was found while creating document
				else {
					return res.status(201).send("New task created");
				}
			})
		}
	})	
});

app.post("/signup", (req, res) => {
  User.findOne({ username: req.body.username }).then((result) => {
    // Check for existing user
    if (result !== null && result.username === req.body.username) {
      return res.send("New user registered");
    } else {
      // Create a new user
      const registerUser = new User({
        username: req.body.username,
        password: req.body.password,
      });
      registerUser.save().then((result) => {
        res.send(result);
      });
    }
  });
});


// Get all the tasks 

app.get("/tasks", (req, res) => {
	Task.find({}).then((result, err) => {
		// If an error occurred
		if(err){
			// Will print any errors found in the console
			return console.log(err);
		} 
		// If no errors are found
		else{
			return res.status(200).json({
				data:result
			})
		}
	})
})

// listen to the port
app.listen(port,() => console.log(`Server is running at ${port}`));