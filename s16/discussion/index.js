// Javascript Operators 

// Arithmethic Operators

let x = 1397;
let y = 7831; 

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y 
console.log("Result of division operator: " + quotient);

let remainder = y % x
console.log("Result of modulo operator " + remainder);

// Assignment Operators (=) - assigns the value of the right oeprand to a variable. 

let assignmentNumber = 8;

// Addition Assignment Operator (+=)

assignmentNumber = assignmentNumber + 2; 
console.log("Result of addition assignment operator: " + assignmentNumber);

// Shorthand 

assignmentNumber += 2; 
console.log("Result of addition assignment operator: " + assignmentNumber);

// Subtraction/Multiplication/Division Assignment Operator (-=) (*=) (/=)

assignmentNumber -= 2;
console.log("Result of subtraction assignment operator " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of multiplication assignment operator " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of division assignment operator " + assignmentNumber);

// Multiple Operators and Parentheses 

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

// Using Paranthesis 

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation " + pemdas);

pemdas = (1 + (2 - 3)) * (4 / 5);
console.log("Result of pemdas operation " + pemdas); 

// Increment and Decrement (++/--) adding 1 value 
let z = 1;

let increment = ++z
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z );

// post-increment 
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z );

let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

decrement = z--
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z );


// Type Coercion - adding two different types / in boolean true is 1 and false is 0 

let numA = "10"; 
let numB = 12; 

let coercion = numA + numB; 
console.log(coercion);
console.log(typeof coercion);

let numC = true + 1;
console.log(numC);

// Comparison operators 

let juan = 'juan';

//  Equality operator (==)

console.log(1==1);
console.log(1==2);
console.log(1=='1');
console.log(0 ==false);
console.log('juan'== juan);

// Inequality operator (!=)

console.log(1!=1);
console.log(1!=2);
console.log(1!='1');
console.log(0 !=false);
console.log('juan'!= juan);

// Strict Equality Operator (===)

console.log(1===1);
console.log(1===2);
console.log(1==='1');
console.log(0 ===false);
console.log('juan'=== juan);

// Strict Inequality (!==)

console.log(1!==1);
console.log(1!==2);
console.log(1!=='1');
console.log(0 !==false);
console.log('juan'!== juan);

// Relational Operators 

let a = 50;
let b = 65;

// Greater than (>)

let isGreaterThan = a > b;

// Less than (<)

let isLessThan = a < b;

// Greater than or equal (>=)

let isGTorEqual = a >= b;

// Less than or equal (<=)

let isLTorEqual = a <= b; 

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

// Logical Operators

let isLegalAge = true; 
let isRegistered = false;

// Logical AND operator (&&) - comparing results of true and false 
// All operands must be true in order to return it as true

let allRequirementsMet = isLegalAge && isRegistered
console.log("Result of Logical AND Operator: " + allRequirementsMet);

// Logical OR Operator (||)
// It will return true if one of the operands is true. 

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of Logical OR Operator: " + someRequirementsMet);

// Logical NOT Operator (!)
// Returns the opposite value

let someRequirementsNotMet = ! isRegistered;
console.log("Result of Logical NOT Operator: " + someRequirementsNotMet)



