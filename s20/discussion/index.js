// While Loops 

/* it evaluates a condition, if returned true, it will execute statements as long as the condition is satisfied. If at first condition is not satisfied, no statement will be executed. 
Syntax: 
	while(expression/condition){
		statement/s;
	}
*/
console.log("While Loop")
let count = 5;

while(count !==0){
	console.log("While: " + count);
	count--;
}
/* 
count: 5, 4, 3 , 2 , 1
Console: 
	While: 5
	While: 4
	While: 3
	While: 2
	While: 1
*/
 
let count2 = 1

while(count2 !==6){
	console.log("While: " + count2)
	count2 ++;
}

// Do-While Loop

/* iterates statements within a number of times based on a condition. However, if the condition was not satisfied at first, one statement will be executed. 
Syntax: 
	do{
		statement/s;
	} while(expression/condition);
*/

/*let number = Number(prompt("Give me a number"));

do{
	console.log("Do While: " + number);
	number += 1; 
	// number = number + 1;
} while(number<10);*/
console.log("Do while Loop")
let even = 2; 

do{
	console.log("Do While: " + even);
	even += 2;
} while(even <= 10);

/* For Loop 
 - a looping construct that is more flexible than other loops. It consists of three parts: 
 	1. Initialization
 	2. Expression/condition
 	3. FInal expression/step expression

 Syntax: 
 	for(initialization; expression/condition; finalExpression){
	statement:
 	}
*/
console.log("For Loops")

for(let count =0; count <= 20; count++){
	console.log(count)
}

/*
	count = 0
	console: 
		0
		1
		2
		3
		4
		5
*/

let myString = "alex";

console.log(myString.length);

// Accessing elements of a string

console.log(myString[0]);
console.log(myString[2]);
console.log(myString[3]);

console.log("Looping through array index")
for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}
//  x is based on myString = alex / index of an array always starts with 0. 

let myName = "AlExis";

console.log("Looping through vowels and consonants")

for(let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" || 
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
	){
		console.log(3)
	} else {
		console.log(myName[i]);
	}
}

//  Continue and Break Statements
console.log("Continue and Break Statements");
for(let count = 0; count <= 20; count++){
	if(count % 2 === 0){
		// tells code to continue the next iteration of the loop
		continue;
	}
	console.log("Continue and Break: " + count);

	// if current value of count is greater than 10, stops the loop. 
	if(count>10){

	//  tells the code to terminate the loop even if the statement is still true. 
		break;
	}
}

// if true, continue will go back to top. if not, it will proceed with console.log > if > break

let name = "Alexandro"
for (let i = 0; i < name.length; i++){
	console.log(name[i]);

	if (name[i].toLowerCase() === "a"){
	console.log("Continue to the next iteration");
	continue;
	}
	//  IF the current letter is equal to 'd', stop the loop
	if(name[i] == "d"){
		break;
	} 
}