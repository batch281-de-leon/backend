const jwt = require ("jsonwebtoken");

// User defined string data that will be used to create our JSON web tokens
// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret keyword

const secret = "CourseBookingAPI"

// JSON WEB TOKENS

module.exports.createAccessToken = (user) => {

	// When the user logs in, a token will be created with the user's information

	const data = {
		id: user._id, 
		email: user.email,
		isAdmin: user.isAdmin
	};
	// Generating JSON Web Token using the jwt's sign method
	return jwt.sign(data, secret, {});
}

// Token Verification

module.exports.verify = (req, res, next) => {
	// token is retrieved from the request header
	// can be provided in postman under authorization > Bearer token
	let token = req.headers.authorization;

	if(typeof token !== "undefined"){
		console.log(token);

		// "Slice" method takes only the token from the information sent via the request header
		// Token sent is a type of "Bearer" token which when received contains the word "Bearer" as a prefix
		// This removes the "Bearer" prefix and only obtains the token
		token = token.slice(7, token.length)

		// Validate the token using the "verify" method decrypting the token using secret code
		return jwt.verify(token, secret, (err,data) =>{
			if (err) {
				return res.send({auth: "failed"})
			} else {
				next();
			}
		})
		// Token does not exists
	} else {
		return res.send({auth: "failed"})
	};
};

// Token decryption 

module.exports.decode = (token) => {

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err,data) =>{
			if (err){
				return null;
			} else {
				// "decode" method used to obtain the information the JWT
				// "{complete:true}" option allows us to return additional information from the JWT
				// returns an object with access to the "payload" property which contains user information stored when the token was generated.
				return jwt.decode(token, {complete:true}).payload;

			}
		})
	} else {
		return null
	};
};


