const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Route for creating a course
router.post("/", auth.verify, (req, res) => {
	
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}

});


// Route for retrieving all the courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving all the active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving a specific course
// Creating a course using "/:parametername" creates a dynamic route, meaning the url changes depending on the information provided
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);

	// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the url
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// ACTIVITY 

// ACTIVITY
router.put("/:courseId/archive", auth.verify, (req,res) => {
    const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
    if(data.isAdmin == true){
        courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
    }else {
        res.send(false)
    }
    
});


// Allows us to export the "router" object that will be accessed in our index.js file
module.exports = router;
