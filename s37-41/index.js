const express = require("express");
const mongoose = require("mongoose");

// initialization of express function

const cors = require ("cors");
const userRoutes = require ("./routes/userRoutes");
const courseRoutes = require ("./routes/courseRoutes");
const app = express(); 

// MongoDB Connection

mongoose.connect("mongodb+srv://mikaellajdeleon:zNOIjPKcWp72KCSD@wdc028-course-booking.vww2uoq.mongodb.net/s37-41API?retryWrites=true&w=majority", {
	useNewUrlParser: true, 
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', console.log.bind(console, 'MongoDB connection error.'))
db.once('open', ()=> console.log('Now connected to MongoDB Atlas!'))

// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.json({extended:true}));

// Defines the "/users string to be included for all user routes defined in the "userRoutes" file

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);


app.listen(process.env.PORT || 4000, () =>{
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});