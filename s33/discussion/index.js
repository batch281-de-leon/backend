// console.log("Hello, World!");

// [Section] Javascript Synchronous vs Asynchronous

// JavaScript is by default synchronous meaning that only one statement is executed at a time.

// This can be proven when a statement has an error, javascript will not proceed with the next statement

console.log("Hello!");
// conosle.log("Hello, again!");
console.log("Goodbye");

// When a certain statements takes a lot of time to process, this slows down our code
// An example of this are when loops are used on a large amount of information or when fetching data from databases
// When an action will take some time to process, this results in code "blocking"
// We might not notice it due to fast processing of our device
// This is the reason why some website don't instantly load and we see a white screen at time while application is still waiting for all the code to be executed. 

// Aynchronous means that we can proceed to execute other statements, while time consuming code is running is the background.

// Create a simple fetch request

// [Section] Getting all posts

// The FETCH API allows you to asynchronously request for a resouce(data)
// A "promise" is an object that represents the eventual completion(failure) of an asynchronous function and it's resulting value. 

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

// Check the status of the request 

// Retrieve all posts following the REST API(Retrieve,/posts, GET)
// By using the then method we can now check the status of the promise
fetch("https://jsonplaceholder.typicode.com/posts")

// The "fetch" method will return a "promise" that resolves to a "Response" object

// The "then" method captures the "response" object and returns another "promise" which will eventually be "resolved" or "rejected"

.then(response => console.log(response.status));

fetch("https://jsonplaceholder.typicode.com/posts")
// Use the 'json' method from the "response" object to convert data retrieved into JSON format to be used in our collection
.then((response) => response.json())
// Print the converted JSON value from the "fetch" request

// Using multiple "then" methods creates a promise chain
.then((json) => console.log(json));

// Demonstrate using the "aysnc" and "await" keywords

// The "ASYNC" and "AWAIT" keyword is another approach that can be used to achieve aysnchronous code
// Used in function to indicate which portions of code should be waited for
// Creates asynchronous function
// Syntax:
async function fetchData(){
	// waits for the "fetch" method to complete then stores the value in "result" variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts")
	// Result returned by fetch returns a promise
	console.log(result);
	// The return "response" is an object. 
	console.log(typeof result);
	// We cannot access the content of "response" by directly acessing its body's property
	console.log(result.body);

	// Converts data from the "Response" object as JSON

	let json = await result.json();
	// Print out the content of the "response object"
	console.log(json);
};

fetchData();

// Process a GET request using postman

/*
 postman: 
 url: https://jsonplaceholder.typicode.com/posts
 method: GET
*/


// [Section] Getting a specific post

// Retrieve specific post following the REST API(retrieve, /posts/:id, GET)

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json));

// Creates a new post following the REST Api(create, /posts/:id, POST)

fetch("https://jsonplaceholder.typicode.com/posts" , {
	// sets the method of "request" object to "post" following the REST API
	// Default method is GET
	method:'POST',
	// Sets the method of the "request" object to "POST" following the REST API
	// Specified that the content will be in a JSON structure
	headers: {
		'Content-Type':'application/json',

	}, 
	// Sets the content/body date of the "request" object to be sent to the backend
	// JSON.stringify converts the object data into a stringified JSON
	body: JSON.stringify({
		title: "New post",
		body: "Hello, World!",
		userID: 1
	}),
})

.then((response) => response.json())
.then((json) => console.log(json));

// [Section] updating a post
// updates a specific post following the REST API (update, /posts/;id, PUT)

fetch("https://jsonplaceholder.typicode.com/posts/1" , {

	method:'PUT',

	headers: {
		'Content-Type':'application/json',

	}, 

	body: JSON.stringify({
		title: "Updated post",
		body: "Hello again!",
		userID: 1
	}),
})

.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Updating a post using PATCH

// Updates a specific post following the REST Api (update, /posts/:id, Patch)
// Difference between PUT and PATCH is the number of properties being changed.

// PUT is used to update the whole object
// PATCH is used to update a single/several properties

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: "PATCH",
	headers: {
		'Content-Type': 'application/json',
	},
	body: JSON.stringify({
		title: 'Corrected post',
	})
})

.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Deleting a post

// Deleting a specific post following the REST API 
fetch('https://jsonplaceholder.typicode.com/posts/1' , {
	method: "DELETE"
});

// [Section] Deleting a post

// Deleting a post following the REST API 
fetch('https://jsonplaceholder.typicode.com/posts' , {
	method: "DELETE"
});
// [Section] Filtering posts

// The data can be filtered by sending the userId along with URL
// Information sent via the URL can be done by adding the question mark symbol
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Retrieving nested/related comments to posts

// Retrieving comments for a specific post following the REST API (retrieve, /posts/:id/comments)

fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((json) => console.log(json));

