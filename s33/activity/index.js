// Note: don't add a semicolon at the end of then().
// Fetch answers must be inside the await () for each function.
// Each function will be used to check the fetch answers.
// Don't console log the result/json, return it.

// Get Single To Do [Sample]
async function getSingleToDo() {
    return await //add fetch here.
        (
            fetch("<urlSample>")
                .then((response) => response.json())
                .then((json) => json)
        );
}

// Getting all to do list item
async function getAllToDo() {
    return await (
        fetch("https://jsonplaceholder.typicode.com/todos")
            .then((response) => response.json())
            .then((json) => json.map((item) => item.title))
            .then((json) => json)
    );
}

getAllToDo().then((data) => console.log(data));

// [Section] Getting a specific to do list item
async function getSpecificToDo() {
    return await (
        fetch("https://jsonplaceholder.typicode.com/todos/1")
            .then((response) => response.json())
            .then((json) => json)
    )
}

getSpecificToDo().then((data) => console.log(data));

// [Section] Creating a to do list item using POST method
async function createToDo() {
    return await (
        fetch("https://jsonplaceholder.typicode.com/todos", {
            method: "POST",
            body: JSON.stringify({
                title: "Created To Do List Item",
                completed: false,
                userId: 1,
            }),
            headers: {
                "Content-type": "application/json",
            },
        })
            .then((response) => response.json())
            .then((json) => json)
    );
}

createToDo().then((data) => console.log(data));

// [Section] Updating a to do list item using PUT method
async function updateToDo() {
    return await (
        fetch("https://jsonplaceholder.typicode.com/todos/1", {
            method: "PUT",
            body: JSON.stringify({
                dateCompleted: "Pending",
                description:
                    "To update the my to do list with a different data structure",
                status: "Pending",
                title: "Updated To Do List Item",
                userId: 1,
            }),
            headers: {
                "Content-type": "application/json",
            },
        })
            .then((response) => response.json())
            .then((json) => json)
    );
}

updateToDo().then((data) => console.log(data));

// [Bonus Section] Updating a to do list item using PATCH method
async function patchToDo() {
    return await (
        fetch("https://jsonplaceholder.typicode.com/todos/1", {
            method: "PATCH",
            body: JSON.stringify({
                completed: false,
                dateCompleted: "07/09/21",
                status: "Complete"
            }),
            headers: {
                "Content-type": "application/json",
            },
        })
            .then((response) => response.json())
            .then((json) => json)
    );
}

patchToDo().then((data) => console.log(data));

// [Section] Deleting a to do list item
async function deleteToDo() {
    // Delete it and console the deleted object
    return await (
        fetch("https://jsonplaceholder.typicode.com/todos/1", {
            method: "DELETE",
        })
            .then((response) => response.json())
            .then((json) => json)
    );
}

deleteToDo();

//Do not modify
//For exporting to test.js
try {
    module.exports = {
        getSingleToDo: typeof getSingleToDo !== "undefined" ? getSingleToDo : null,
        getAllToDo: typeof getAllToDo !== "undefined" ? getAllToDo : null,
        getSpecificToDo:
            typeof getSpecificToDo !== "undefined" ? getSpecificToDo : null,
        createToDo: typeof createToDo !== "undefined" ? createToDo : null,
        updateToDo: typeof updateToDo !== "undefined" ? updateToDo : null,
        deleteToDo: typeof deleteToDo !== "undefined" ? deleteToDo : null,
    };
} catch (err) { }

