const express = require("express");

const port = 3000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Mock database
let users = [
  {
    username: "johndoe",
    password: "johndoe1234",
  },
];

// GET route that will access /home route and send a welcome message
app.get("/home", (req, res) => {
  res.send("Welcome to the home page");
});

// GET route that will access /users and send the users array as a response
app.get("/users", (req, res) => {
  res.send(users);
});

// DELETE route that will access /users and locate the user by their username and delete them from the users array
app.delete("/users", (req, res) => {
  let message;
  for (let i = 0; i < users.length; i++) {
    if (users[i].username === req.body.username) {
      users.splice(i, 1);
      message = `User ${req.body.username} has been deleted.`;
      break;
    } else message = "User not found!";
  }
  res.send(message);
});

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
