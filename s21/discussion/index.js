// Array Traversal - used to store multiple values for the same data type

let grades = [98.5, 94.3, 89.2, 90.1];

// index = actual location of elements
// elements = actual value of the array

let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];

let mixArr = [12, 'Asus', null, undefined]; // defeats the purpose of an array. not recommended. 

let tasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
];

// Creating an array with values from variables
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Mumbai";

let cities = [city1, city2, city3];
console.log(tasks)
console.log(cities)

// Array length property 
console.log("Array length");
console.log(tasks.length);
console.log(cities.length);

let fullName = "Aizaac Estiva";
console.log(fullName.length);

tasks.length = tasks.length - 1;
console.log(tasks.length);
console.log(tasks);

cities.length--;
console.log(cities);

fullName.length = fullName.length -1; // this will not work
console.log(fullName.length);
console.log(fullName);

// It will not work on strings. It must be array to be able to reduce elements. 

// Adding a number to lengthen the size of an array

let theBeatles = ["John", "Paul","Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);

// Accessing elements of an array ; 

console.log(grades[0]);
console.log(computerBrands[3]);
console.log(grades[20]); // will not result to error just undefined (exceeeds the number of elements declared in array)

let lakerLegends = ["Kobe","Shaq", "LeBron", "Magic", "Kareem"];
console.log(lakerLegends[1]);
console.log(lakerLegends[3]);
// index = numberItem - 1; (index is [i])

let currentLaker = lakerLegends[2];
console.log("Accesing arrays using variables")
console.log(currentLaker);

// Reassigning values in array
console.log("Array before reassignment");
console.log(lakerLegends);
lakerLegends[2] = "Pau Gasol";
console.log("Array after reassignment");
console.log(lakerLegends);

//  Accessing the last elements of an array 
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegends.length -1;

console.log(bullsLegends[lastElementIndex]); // using variables

// Directly access the expression

console.log(bullsLegends[bullsLegends.length-1]); // using expression

//  total length start with 1. index starts with 0. 

// Adding items into the array 
console.log("Adding items in array")
let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife"; // assignment of data 
console.log(newArr[0]);

let newVar = prompt("Enter a name: ")
newArr[1] = newVar;
console.log(newArr);

// Looping through array

for(let index=0;index < computerBrands.length; index++){
	console.log(computerBrands[index]);
}

// Checks the elements if it's divisible by 5. 

let numArr = [5, 12 , 30, 46, 40]

for(let index = 0; index < numArr.length; index++){
	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " divisible by 5");
	} else {
		console.log(numArr[index] + " is not divisible by 5");
	}
}

// Multidimensional Array

//  2 x 2 (Two dimensional array) 
/*
 let twoDim = [[elem1, elem2],[elem3, elem4]]
 elem 1 and 2 is index 0 / if inside, elem1 is 0 and elem 2 is 1
 elem 3 and 4 is index 1 / if inside, elem3 is 0 and elem4 is 1

 twoDim[0][0];
 twoDim[1][0];
*/

let twoDim = [["Kenzo","Alonzo"],["Bella","Aizaac"]]
console.log(twoDim[0][1]);
console.log(twoDim[1][0]);
console.log(twoDim[1][1]);

let twoDimLen = twoDim.length;
console.log(twoDimLen);